#!/bin/sh
# Constants
REPO_URL="http://cdn.bitbucket.org/jcros/repository/downloads"
# Begin Actual Script
echo "Welcome to the JCros Oracle JDK 7 installer...."
sleep 1
#### Begin Script Functions ####
get_install_path() {
    # Check to see where user wants to install JDK 7
    read -p "Please enter install path (Default: /usr/local/jvm/): " JDK_INSTALL_PATH
    # Print install path
    if [ -z ${JDK_INSTALL_PATH} ] ; then echo "Installing JDK in /usr/local/jvm/"; else echo "Installing JDK in " ${JDK_INSTALL_PATH}; fi
    # Check if it is correct, and if not then ask for path again
    read -p "Is this correct? (yN) " LINE ; [ "$LINE" != "y" ] && get_install_path
}
# Not yet used
get_install_doc() {
    read -p "Install Documentation? (yN) " INSTALL_DOC
}
# Not yet used
get_install_appstore() {
    read -p "Install JCros App Store? (yN) " INSTALL_APP_STORE
}
get_latest_jre_version() {
    LATEST_VERSION_JRE_URL="https://bitbucket.org/jcros/repository/raw/master/jre-latest"
    LATEST_VERSION_JRE=`wget --quiet -O- ${LATEST_VERSION_JRE_URL}`
}
#### End Script Functions ####
# Run Configuration Options
get_install_path
# get_install_doc
# get_install_appstore
get_latest_jre_version
JRE_URL="${REPO_URL}/jre-"${LATEST_VERSION_JRE}"-linux-x64.tar.gz"
JRE_FILENAME="jre-"${LATEST_VERSION_JRE}"-linux-x64.tar.gz"
wget ${JRE_URL} -O ${JRE_FILENAME}
